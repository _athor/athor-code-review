#include "athorcodereviewcheck.h"

#include <QDebug>

#include <request/athorgitlabmergerequestrequest.h>
#include <data/athorgitlabmergerequestdiscussionnotemodel.h>
#include <request/athorgitlabmergerequestdiscussionrequest.h>
#include <data/athorgitlabmergerequestdiscussionnoteauthormodel.h>

#include "mergerequestdiffregex.h"
#include "mergerequesttitleregex.h"
#include "mergerequestbranchnamecompare.h"
#include "mergerequestdiffuncrusify.h"

using athor::gitlab::api::AthorGitlabMergeRequestModel;
using athor::gitlab::api::AthorGitlabMergeRequestRequest;
using athor::gitlab::api::AthorGitlabMergeRequestDiscussionModel;
using athor::gitlab::api::AthorGitlabMergeRequestDiscussionRequest;
using athor::gitlab::api::AthorGitlabMergeRequestDiscussionNoteModel;
using athor::gitlab::api::AthorGitlabMergeRequestDiscussionNoteAuthorModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

AthorCodeReviewCheck::~AthorCodeReviewCheck() {
    qDeleteAll( _discussionList );
    _discussionList = {};

    if ( _athorGitlabMergeRequest ) {
        delete _athorGitlabMergeRequest;
        _athorGitlabMergeRequest = nullptr;
    }
}

void AthorCodeReviewCheck::check( const int& projectId, const int& mergeIid ) {

    qDebug() << "AthorCodeReviewCheck::check";
    checkConfigIsOk( projectId, mergeIid );

    qDebug() << "AthorCodeReviewCheck::check getSingle";
    _athorGitlabMergeRequest = AthorGitlabMergeRequestRequest( _gitLabUrl, _privateToken ).getSingle( projectId, mergeIid );

    if ( _typeList.contains( MergeRequestBranchNameCompare::TYPE ) ) {
        qDebug() << "AthorCodeReviewCheck::check MergeRequestBranchNameCompare";
        _discussionList.append( MergeRequestBranchNameCompare().check( _athorGitlabMergeRequest ) );
    }

    if ( _typeList.contains( MergeRequestTitleRegex::TYPE ) ) {
        qDebug() << "AthorCodeReviewCheck::check MergeRequestTitleRegex";
        MergeRequestTitleRegex mergeRequestTitleRegex;
        mergeRequestTitleRegex.setPathConfig( _pathConfig );

        _discussionList.append( mergeRequestTitleRegex.check( _athorGitlabMergeRequest ) );
    }

    if ( _typeList.contains( MergeRequestDiffRegex::TYPE ) ) {
        qDebug() << "AthorCodeReviewCheck::check MergeRequestDiffRegex";
        MergeRequestDiffRegex mergeRequestDiffRegex;
        mergeRequestDiffRegex.setGitLabUrl( _gitLabUrl );
        mergeRequestDiffRegex.setPathConfig( _pathConfig );
        mergeRequestDiffRegex.setPrivateToken( _privateToken );

        mergeRequestDiffRegex.check( _athorGitlabMergeRequest );

        _commentList.append( mergeRequestDiffRegex.commentList() );
        _discussionList.append( mergeRequestDiffRegex.discussionList() );
    }

    if ( _typeList.contains( MergeRequestDiffUncrusify::TYPE ) ) {
        qDebug() << "AthorCodeReviewCheck::check MergeRequestDiffUncrusify";
        MergeRequestDiffUncrusify mergeRequestDiffUncrusify;
        mergeRequestDiffUncrusify.setGitLabUrl( _gitLabUrl );
        mergeRequestDiffUncrusify.setPathConfig( _pathConfig );
        mergeRequestDiffUncrusify.setPrivateToken( _privateToken );

        mergeRequestDiffUncrusify.check( _athorGitlabMergeRequest );

        _commentList.append( mergeRequestDiffUncrusify.commentList() );
        _discussionList.append( mergeRequestDiffUncrusify.discussionList() );
    }

    publish( projectId, mergeIid );

}

QString AthorCodeReviewCheck::gitLabUrl() const {
    return _gitLabUrl;
}

void AthorCodeReviewCheck::setGitLabUrl( const QString& gitLabUrl ) {
    _gitLabUrl = gitLabUrl;
}

QStringList AthorCodeReviewCheck::typeList() const {
    return _typeList;
}

void AthorCodeReviewCheck::setTypeList( const QStringList& typeList ) {
    _typeList = typeList;
}

QString AthorCodeReviewCheck::privateToken() const {
    return _privateToken;
}

void AthorCodeReviewCheck::setPrivateToken( const QString& privateToken ) {
    _privateToken = privateToken;
}

QString AthorCodeReviewCheck::pathConfig() const {
    return _pathConfig;
}

void AthorCodeReviewCheck::setPathConfig( const QString& pathConfig ) {
    _pathConfig = pathConfig;
}

int AthorCodeReviewCheck::idUser() const {
    return _idUser;
}

void AthorCodeReviewCheck::setIdUser( int idUser ) {
    _idUser = idUser;
}

AthorGitlabMergeRequestModel* AthorCodeReviewCheck::athorGitlabMergeRequest() const {
    return _athorGitlabMergeRequest;
}

void AthorCodeReviewCheck::publish( const int& projectId, const int& mergeIid ) {

    qDebug() << "AthorCodeReviewCheck::publish";
    QList<AthorGitlabMergeRequestDiscussionModel*> discussionList = AthorGitlabMergeRequestDiscussionRequest( _gitLabUrl, _privateToken ).getListByMerge( projectId, mergeIid );

    for ( const AthorGitlabMergeRequestDiscussionModel* discussion : discussionList ) {
        AthorGitlabMergeRequestDiscussionNoteModel* note = discussion->notes().at( 0 );

        if ( !note->resolved() && note->author()->id() == _idUser ) {
            QString body = discussion->notes().at( 0 )->body();

            if ( body.startsWith( "SONAR_QUBE_ISSUES" ) ) {

                continue;

            }

            if ( !hasDiscussionByBody( body ) ) {
                AthorGitlabMergeRequestDiscussionRequest( _gitLabUrl, _privateToken ).updateResolved( projectId, mergeIid, discussion->id(), true );
            }
        }
    }

    for ( const AthorCodeReviewDiscussionModel* discussion : _discussionList ) {
        bool found = false;

        for ( const AthorGitlabMergeRequestDiscussionModel* discussionGitLab : discussionList ) {
            AthorGitlabMergeRequestDiscussionNoteModel* note = discussionGitLab->notes().at( 0 );

            if ( note->body() == discussion->body() ) {
                found = true;
                break;
            }
        }

        if ( !found ) {
            AthorGitlabMergeRequestDiscussionRequest( _gitLabUrl, _privateToken ).insert( projectId, mergeIid, discussion->body() );
        }
    }

}

bool AthorCodeReviewCheck::hasDiscussionByBody( const QString& body ) const {

    for ( AthorCodeReviewDiscussionModel* discussion : _discussionList ) {
        if ( discussion->body() == body ) {
            return true;
        }
    }

    return false;

}

void AthorCodeReviewCheck::checkConfigIsOk( const int& projectId, const int& mergeIid ) const {
    qDebug() << "AthorCodeReviewCheck::checkConfigIsOk";

    if ( _gitLabUrl.isEmpty() ) {
        qCritical() << "AthorCodeReviewCheck::checkConfigIsOk gitLabUrl";
        throw new std::runtime_error( "gitLabUrl is empty" );
    }

    if ( _privateToken.isEmpty() ) {
        qCritical() << "AthorCodeReviewCheck::checkConfigIsOk privateToken";
        throw new std::runtime_error( "privateToken is empty" );
    }

    if ( projectId <= 0 ) {
        qCritical() << "AthorCodeReviewCheck::checkConfigIsOk projectId";
        throw new std::runtime_error( "projectId invalid" );
    }

    if ( mergeIid <= 0 ) {
        qCritical() << "AthorCodeReviewCheck::checkConfigIsOk mergeIid";
        throw new std::runtime_error( "mergeIid invalid" );
    }
}

ATHOR_CODE_REVIEW_API_END_NAMESPACE
