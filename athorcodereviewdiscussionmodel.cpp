#include "athorcodereviewdiscussionmodel.h"

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

AthorCodeReviewDiscussionModel::AthorCodeReviewDiscussionModel() {}

QString AthorCodeReviewDiscussionModel::body() const {
    return _body;
}

void AthorCodeReviewDiscussionModel::setBody( const QString& body ) {
    _body = body;
}

ATHOR_CODE_REVIEW_API_END_NAMESPACE
