#include "athorcodereviewdiscussionmodel.h"

#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDirIterator>
#include <QDebug>

#include <request/athorgitlabmergerequestversionrequest.h>

#include "mergerequestdiffregex.h"

using athor::gitlab::api::AthorGitlabMergeRequestVersionModel;
using athor::gitlab::api::AthorGitlabMergeRequestVersionRequest;
using athor::gitlab::api::AthorGitlabMergeRequestVersionDiffModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

QStringList MergeRequestDiffRegex::diffListByMerge( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel, const QString& sourceMerge ) const {

    QStringList pathList = {};
    QList<AthorGitlabMergeRequestVersionModel*> versionList = AthorGitlabMergeRequestVersionRequest( _gitLabUrl, _privateToken ).getMergeDiffVersion( athorGitlabMergeRequestModel->projectId(), athorGitlabMergeRequestModel->iid() );

    for ( const AthorGitlabMergeRequestVersionModel* version : versionList ) {
        QList<AthorGitlabMergeRequestVersionDiffModel*> diffList = AthorGitlabMergeRequestVersionRequest( _gitLabUrl, _privateToken ).getSingleMergeDiffVersion( athorGitlabMergeRequestModel->projectId(), athorGitlabMergeRequestModel->iid(), version->id() );

        for ( const AthorGitlabMergeRequestVersionDiffModel* diff : diffList ) {
            QString path = QString( sourceMerge ).append( diff->newPath() );

            if ( !pathList.contains( path ) ) {
                pathList.append( path );
            }
        }

        qDeleteAll( diffList );
    }

    qDeleteAll( versionList );

    return pathList;

}

void MergeRequestDiffRegex::check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) {

    checkConfigIsOk( athorGitlabMergeRequestModel );
    QFile file( _pathConfig + QDir::separator() + "merge-request-diff-regex.json" );

    if ( !file.open( QFile::ReadOnly ) ) {
        throw new std::runtime_error( "merge-request-diff-regex fail to open" );
    }

    QJsonObject json = QJsonDocument::fromJson( file.readAll() ).object();
    QString sourceMerge = json["sourceMerge"].toString().append( QDir::separator() );

    if ( sourceMerge.isEmpty() ) {
        throw new std::runtime_error( "merge-request-diff-regex source merge is empty" );
    }

    QStringList diffList = diffListByMerge( athorGitlabMergeRequestModel, sourceMerge );
    QJsonArray regexCheckList = json["regexCheckList"].toArray();

    if ( regexCheckList.isEmpty() ) {
        throw new std::runtime_error( "regex check list is empty" );
    }

    qInfo() << "MergeRequestDiffRegex::check [QT_REGEX_CHECK]" << regexCheckList.count() << "[QT_DIFF]" << diffList.count();

    for ( const QString& diff : diffList ) {
        qInfo() << "MergeRequestDiffRegex::check [DIFF]" << diff;

        QFile checkFile( diff );
        checkFile.open( QFile::ReadOnly );

        for ( QJsonValue regexCheck : regexCheckList ) {
            QJsonObject object = regexCheck.toObject();

            QString dsFileName = checkFile.fileName();
            QString dsRegexName = object["regexName"].toString();
            bool exactMatch = QRegExp( dsRegexName ).exactMatch( dsFileName );

            qInfo() << "MergeRequestDiffRegex::check [REGEX_NAME]" << dsRegexName << "[FILE_NAME]" << dsFileName << "[EXACT_MATH]" << exactMatch;

            if ( exactMatch ) {
                QJsonArray regexListArray = object["regexList"].toArray();
                qInfo() << "MergeRequestDiffRegex::check [QT_REGEX_LIST]" << regexListArray.count();

                for ( QJsonValue regex : regexListArray ) {
                    QString regexToCheck = regex.toString();
                    QString readAllToCheck = QString( checkFile.readAll() );
                    bool addComment = readAllToCheck.contains( QRegExp( regexToCheck ) );
                    qInfo() << "MergeRequestDiffRegex::check [REGEX_TO_CHECK]" << regexToCheck << "[ADD_COMMENT]" << addComment;
                    qDebug() << "MergeRequestDiffRegex::check [READ_ALL_TO_CHECK]" << readAllToCheck;

                    if ( addComment ) {
                        QString text = QString( object["text"].toString() );
                        text = text.replace( "${FILE_PATH}", QString( diff ).remove( sourceMerge ) );

                        AthorCodeReviewDiscussionModel* discussion = new AthorCodeReviewDiscussionModel();
                        discussion->setBody( text );

                        _discussionList.append( discussion );
                        break;
                    }
                }
            }
        }
    }

}

QString MergeRequestDiffRegex::pathConfig() const {
    return _pathConfig;
}

void MergeRequestDiffRegex::setPathConfig( const QString& pathConfig ) {
    _pathConfig = pathConfig;
}

QList<AthorCodeReviewDiscussionModel*> MergeRequestDiffRegex::commentList() const {
    return _commentList;
}

QList<AthorCodeReviewDiscussionModel*> MergeRequestDiffRegex::discussionList() const {
    return _discussionList;
}

QString MergeRequestDiffRegex::gitLabUrl() const {
    return _gitLabUrl;
}

void MergeRequestDiffRegex::setGitLabUrl( const QString& gitLabUrl ) {
    _gitLabUrl = gitLabUrl;
}

QString MergeRequestDiffRegex::privateToken() const {
    return _privateToken;
}

void MergeRequestDiffRegex::setPrivateToken( const QString& privateToken ) {
    _privateToken = privateToken;
}

void MergeRequestDiffRegex::checkConfigIsOk( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const {
    if ( _pathConfig.isEmpty() ) {
        throw new std::runtime_error( "path config is empty" );
    }

    if ( _gitLabUrl.isEmpty() ) {
        throw new std::runtime_error( "gitLabUrl is empty" );
    }

    if ( _privateToken.isEmpty() ) {
        throw new std::runtime_error( "privateToken is empty" );
    }

    if ( !athorGitlabMergeRequestModel ) {
        throw new std::runtime_error( "gitlab merge request model is invalid" );
    }
}

ATHOR_CODE_REVIEW_API_END_NAMESPACE
