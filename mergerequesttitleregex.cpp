#include "athorcodereviewdiscussionmodel.h"

#include <QDir>
#include <QList>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

#include "mergerequesttitleregex.h"

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

bool checkApplyCondition( const QJsonObject& json, const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) {
    for ( QJsonValue condition : json["conditionList"].toArray() ) {
        if ( condition.toObject().contains( "targetBranch" ) ) {
            if ( !QRegExp( condition.toObject()["targetBranch"].toString() ).exactMatch( athorGitlabMergeRequestModel->targetBranch() ) ) {
                return false;
            }
        }
    }

    return true;
}

QList<AthorCodeReviewDiscussionModel*> MergeRequestTitleRegex::check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const {

    checkConfigIsOk( athorGitlabMergeRequestModel );
    QFile file( _pathConfig + QDir::separator() + "merge-request-title-regex.json" );

    if ( !file.open( QFile::ReadOnly ) ) {
        throw new std::runtime_error( "merge-request-title-regex.json fail on open" );
    }

    QList<AthorCodeReviewDiscussionModel*> discussionList = {};
    QString title = athorGitlabMergeRequestModel->title();
    QJsonArray regexCheckList = QJsonDocument::fromJson( file.readAll() ).array();

    if ( regexCheckList.isEmpty() ) {
        throw new std::runtime_error( "regex check list is empty" );
    }

    for ( QJsonValue regexCheck : regexCheckList ) {
        QJsonObject regex = regexCheck.toObject();

        if ( !checkApplyCondition( regex, athorGitlabMergeRequestModel ) ) {
            continue;
        }

        for ( QJsonValue regexString : regex["regexList"].toArray() ) {
            if ( !QRegExp( regexString.toString() ).exactMatch( title ) ) {
                AthorCodeReviewDiscussionModel* discussion = new AthorCodeReviewDiscussionModel();
                discussion->setBody( regex["text"].toString() );

                discussionList.append( discussion );
                break;
            }
        }
    }

    return discussionList;

}

QString MergeRequestTitleRegex::pathConfig() const {
    return _pathConfig;
}

void MergeRequestTitleRegex::setPathConfig( const QString& pathConfig ) {
    _pathConfig = pathConfig;
}

void MergeRequestTitleRegex::checkConfigIsOk( const gitlab::api::AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const {
    if ( _pathConfig.isEmpty() ) {
        throw new std::runtime_error( "path config is empty" );
    }

    if ( !athorGitlabMergeRequestModel ) {
        throw new std::runtime_error( "gitlab merge request model is invalid" );
    }

    if ( athorGitlabMergeRequestModel->title().isEmpty() ) {
        throw new std::runtime_error( "title gitlab merge request is empty" );
    }
}

ATHOR_CODE_REVIEW_API_END_NAMESPACE
