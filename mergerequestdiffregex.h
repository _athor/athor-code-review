#ifndef MERGEREQUESTDIFFREGEX_H
#define MERGEREQUESTDIFFREGEX_H

#include <QList>
#include <athor-code-review_global.h>

#include <data/athorgitlabmergerequestmodel.h>

using athor::gitlab::api::AthorGitlabMergeRequestModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

class AthorCodeReviewDiscussionModel;

class MergeRequestDiffRegex {

public:
    static constexpr const char* TYPE = "merge-request-diff-regex";

    void check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel );

    QString pathConfig() const;
    void setPathConfig( const QString& pathConfig );

    QList<AthorCodeReviewDiscussionModel*> commentList() const;

    QList<AthorCodeReviewDiscussionModel*> discussionList() const;

    QString gitLabUrl() const;
    void setGitLabUrl( const QString& gitLabUrl );

    QString privateToken() const;
    void setPrivateToken( const QString& privateToken );

private:
    QString _gitLabUrl = "";
    QString _pathConfig = "";
    QString _privateToken = "";

    QList<AthorCodeReviewDiscussionModel*> _commentList = {};
    QList<AthorCodeReviewDiscussionModel*> _discussionList = {};

    QStringList diffListByMerge( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel, const QString& sourceMerge ) const;

    void checkConfigIsOk( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const;

};

ATHOR_CODE_REVIEW_API_END_NAMESPACE

#endif // MERGEREQUESTDIFFREGEX_H
