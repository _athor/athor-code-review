#ifndef MERGEREQUESTDIFFUNCRUSIFY_H
#define MERGEREQUESTDIFFUNCRUSIFY_H

#include <QList>

#include <athor-code-review_global.h>

#include <data/athorgitlabmergerequestmodel.h>

using athor::gitlab::api::AthorGitlabMergeRequestModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

class AthorCodeReviewDiscussionModel;

class MergeRequestDiffUncrusify {

public:
    static constexpr const char* TYPE = "merge-request-diff-uncrusify";

    void check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel );

    QList<AthorCodeReviewDiscussionModel*> commentList() const;
    QList<AthorCodeReviewDiscussionModel*> discussionList() const;

    QString pathConfig() const;
    void setPathConfig( const QString& pathConfig );

    QString gitLabUrl() const;
    void setGitLabUrl( const QString& gitLabUrl );

    QString privateToken() const;
    void setPrivateToken( const QString& privateToken );

private:
    QString _gitLabUrl = "";
    QString _pathConfig = "";
    QString _privateToken = "";

    QList<AthorCodeReviewDiscussionModel*> _commentList = {};
    QList<AthorCodeReviewDiscussionModel*> _discussionList = {};

    void checkConfigIsOk( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const;

    QStringList diffListByMerge( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel, const QString& sourceMerge ) const;

    bool checkIsOk( const QString& file, const QString& config, bool& ok );
};

ATHOR_CODE_REVIEW_API_END_NAMESPACE

#endif // MERGEREQUESTDIFFUNCRUSIFY_H
