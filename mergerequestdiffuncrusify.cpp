#include "athorcodereviewdiscussionmodel.h"
#include "mergerequestdiffuncrusify.h"

#include <QDir>
#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>

#include <data/athorgitlabmergerequestversiondiffmodel.h>
#include <request/athorgitlabmergerequestversionrequest.h>

using athor::gitlab::api::AthorGitlabMergeRequestVersionModel;
using athor::gitlab::api::AthorGitlabMergeRequestVersionRequest;
using athor::gitlab::api::AthorGitlabMergeRequestVersionDiffModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

bool MergeRequestDiffUncrusify::checkIsOk( const QString& file, const QString& config, bool& ok ) {

    QProcess qProcess;
    qProcess.start( "uncrustify", {
        "-c", config,
        "-f", file,
        "--check"
    } );

    ok = qProcess.waitForStarted();
    ok &= qProcess.waitForFinished();

    int exitCode = -1;

    if ( ok ) {
        exitCode = qProcess.exitCode();

        qDebug() << "MergeRequestDiffUncrusify::checkIsOk [EXIT_CODE]" << exitCode;
        qDebug() << "MergeRequestDiffUncrusify::checkIsOk [READ_ALL]" << qProcess.readAll();

    } else {
        qDebug() << "MergeRequestDiffUncrusify::checkIsOk [ERROR_STRING]" << qProcess.errorString();
    }

    qDebug() << "MergeRequestDiffUncrusify::checkIsOk [PROGRAM]" << qProcess.program();
    qDebug() << "MergeRequestDiffUncrusify::checkIsOk [ARGUMENTS]" << qProcess.arguments();

    return exitCode == 0;

}

QStringList MergeRequestDiffUncrusify::diffListByMerge( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel, const QString& sourceMerge ) const {

    QStringList pathList = {};
    QList<AthorGitlabMergeRequestVersionModel*> versionList = AthorGitlabMergeRequestVersionRequest( _gitLabUrl, _privateToken ).getMergeDiffVersion( athorGitlabMergeRequestModel->projectId(), athorGitlabMergeRequestModel->iid() );

    for ( const AthorGitlabMergeRequestVersionModel* version : versionList ) {
        QList<AthorGitlabMergeRequestVersionDiffModel*> diffList = AthorGitlabMergeRequestVersionRequest( _gitLabUrl, _privateToken ).getSingleMergeDiffVersion( athorGitlabMergeRequestModel->projectId(), athorGitlabMergeRequestModel->iid(), version->id() );

        for ( const AthorGitlabMergeRequestVersionDiffModel* diff : diffList ) {
            QString path = QString( sourceMerge ).append( diff->newPath() );

            if ( !pathList.contains( path ) ) {
                pathList.append( path );
            }
        }

        qDeleteAll( diffList );
    }

    qDeleteAll( versionList );

    return pathList;

}


void MergeRequestDiffUncrusify::check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) {

    checkConfigIsOk( athorGitlabMergeRequestModel );
    QFile file( _pathConfig + QDir::separator() + "merge-request-diff-uncrusify.json" );

    if ( !file.open( QFile::ReadOnly ) ) {
        throw new std::runtime_error( "merge-request-diff-uncrusify fail to open" );
    }

    QJsonObject json = QJsonDocument::fromJson( file.readAll() ).object();
    QString sourceMerge = json["sourceMerge"].toString().append( QDir::separator() );
    QString config = json["config"].toString();

    if ( sourceMerge.isEmpty() ) {
        throw new std::runtime_error( "merge-request-diff-uncrusify source merge is empty" );
    }

    if ( config.isEmpty() ) {
        throw new std::runtime_error( "merge-request-diff-uncrusify config is empty" );
    }

    QStringList diffList = diffListByMerge( athorGitlabMergeRequestModel, sourceMerge );

    for ( const QString& diff : diffList ) {
        bool ok;

        if ( !diff.endsWith( ".h" ) && !diff.endsWith( ".cpp" ) ) {
            continue;
        }

        if ( !checkIsOk( diff, config, ok ) ) {
            if ( ok ) {
                AthorCodeReviewDiscussionModel* discussion = new AthorCodeReviewDiscussionModel();
                discussion->setBody( QString( "Arquivo %0 com identação incorreta " ).arg( QString( diff ).remove( sourceMerge ) ) );

                _discussionList.append( discussion );

            } else {
                qDebug() << "MergeRequestDiffUncrusify::check fail execute command";
            }
        }
    }

}

QList<AthorCodeReviewDiscussionModel*> MergeRequestDiffUncrusify::commentList() const {
    return _commentList;
}

QList<AthorCodeReviewDiscussionModel*> MergeRequestDiffUncrusify::discussionList() const {
    return _discussionList;
}

QString MergeRequestDiffUncrusify::pathConfig() const {
    return _pathConfig;
}

void MergeRequestDiffUncrusify::setPathConfig( const QString& pathConfig ) {
    _pathConfig = pathConfig;
}

QString MergeRequestDiffUncrusify::gitLabUrl() const {
    return _gitLabUrl;
}

void MergeRequestDiffUncrusify::setGitLabUrl( const QString& gitLabUrl ) {
    _gitLabUrl = gitLabUrl;
}

QString MergeRequestDiffUncrusify::privateToken() const {
    return _privateToken;
}

void MergeRequestDiffUncrusify::setPrivateToken( const QString& privateToken ) {
    _privateToken = privateToken;
}

void MergeRequestDiffUncrusify::checkConfigIsOk( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const {

    if ( _pathConfig.isEmpty() ) {
        throw new std::runtime_error( "path config is empty" );
    }

    if ( _gitLabUrl.isEmpty() ) {
        throw new std::runtime_error( "gitLabUrl is empty" );
    }

    if ( _privateToken.isEmpty() ) {
        throw new std::runtime_error( "privateToken is empty" );
    }

    if ( !athorGitlabMergeRequestModel ) {
        throw new std::runtime_error( "gitlab merge request model is invalid" );
    }

}

ATHOR_CODE_REVIEW_API_END_NAMESPACE
