#ifndef MERGEREQUESTTITLEREGEX_H
#define MERGEREQUESTTITLEREGEX_H

#include <athor-code-review_global.h>

#include <data/athorgitlabmergerequestmodel.h>

using athor::gitlab::api::AthorGitlabMergeRequestModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

class AthorCodeReviewDiscussionModel;

class MergeRequestTitleRegex {

public:
    static constexpr const char* TYPE = "merge-request-title-regex";

    QList<AthorCodeReviewDiscussionModel*> check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const;

    QString pathConfig() const;
    void setPathConfig( const QString& pathConfig );

private:
    QString _pathConfig = "";

    void checkConfigIsOk( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const;

};

ATHOR_CODE_REVIEW_API_END_NAMESPACE

#endif // MERGEREQUESTTITLEREGEX_H
