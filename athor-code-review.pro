#---------------------------------------------------
# Project created by kielsonzinn 2020-03-26T20:53:00
#---------------------------------------------------

QT -= gui
QT += network

TEMPLATE = lib
DEFINES += ATHORCODEREVIEWAPI_LIBRARY

CONFIG(debug, debug|release) {
    DESTDIR = build/debug
}
CONFIG(release, debug|release) {
    DESTDIR = build/release
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.u

ATHOR_GITLAB_API_DIR = ../athor-gitlab-api

INCLUDEPATH += $$ATHOR_GITLAB_API_DIR
LIBS += -L$$OUT_PWD/$$ATHOR_GITLAB_API_DIR/$$DESTDIR -lathor-gitlab-api

SOURCES += \
    athorcodereviewcheck.cpp \
    athorcodereviewdiscussionmodel.cpp \
    athorcodereviewpublishmodel.cpp \
    mergerequestbranchnamecompare.cpp \
    mergerequestdiffregex.cpp \
    mergerequestdiffuncrusify.cpp \
    mergerequesttitleregex.cpp

HEADERS += \
    athor-code-review_global.h \
    athorcodereviewcheck.h \
    athorcodereviewdiscussionmodel.h \
    athorcodereviewpublishmodel.h \
    mergerequestbranchnamecompare.h \
    mergerequestdiffregex.h \
    mergerequestdiffuncrusify.h \
    mergerequesttitleregex.h
