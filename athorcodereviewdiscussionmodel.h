#ifndef ATHORCODEREVIEWDISCUSSIONMODEL_H
#define ATHORCODEREVIEWDISCUSSIONMODEL_H

#include <QString>
#include <athor-code-review_global.h>

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

class AthorCodeReviewDiscussionModel {

public:
    AthorCodeReviewDiscussionModel();

    QString body() const;
    void setBody( const QString& body );

private:
    QString _body = "";

};

#endif // ATHORCODEREVIEWDISCUSSIONMODEL_H

ATHOR_CODE_REVIEW_API_END_NAMESPACE
