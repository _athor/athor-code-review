#ifndef ATHORCODEREVIEWPUBLISHMODEL_H
#define ATHORCODEREVIEWPUBLISHMODEL_H

#include <QList>

#include <athor-code-review_global.h>

#include "athorcodereviewdiscussionmodel.h"

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

class AthorCodeReviewPublishModel {

public:
    AthorCodeReviewPublishModel();
    ~AthorCodeReviewPublishModel();

    QList<AthorCodeReviewDiscussionModel*> discussionList() const;
    void addDiscussion( AthorCodeReviewDiscussionModel* discussion );
    void setDiscussionList( const QList<AthorCodeReviewDiscussionModel*>& discussionList );

private:
    QList<AthorCodeReviewDiscussionModel*> _discussionList = {};

};

#endif // ATHORCODEREVIEWPUBLISHMODEL_H

ATHOR_CODE_REVIEW_API_END_NAMESPACE
