#ifndef ATHORCODEREVIEWCHECK_H
#define ATHORCODEREVIEWCHECK_H

#include <QString>
#include <QStringList>

#include <athor-code-review_global.h>

#include "athorcodereviewdiscussionmodel.h"
#include <data/athorgitlabmergerequestmodel.h>

using athor::gitlab::api::AthorGitlabMergeRequestModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

class AthorCodeReviewCheck {

public:
    ~AthorCodeReviewCheck();

    void check( const int& projectId, const int& mergeIid );

    QString gitLabUrl() const;
    void setGitLabUrl( const QString& gitLabUrl );

    QStringList typeList() const;
    void setTypeList( const QStringList& typeList );

    QString privateToken() const;
    void setPrivateToken( const QString& privateToken );

    QString pathConfig() const;
    void setPathConfig( const QString& pathConfig );

    int idUser() const;
    void setIdUser( int idUser );

    AthorGitlabMergeRequestModel* athorGitlabMergeRequest() const;

private:
    int _idUser = 0;
    QString _gitLabUrl = "";
    QString _pathConfig = "";
    QString _privateToken = "";
    QStringList _typeList = {};
    QList<AthorCodeReviewDiscussionModel*> _commentList = {};
    QList<AthorCodeReviewDiscussionModel*> _discussionList = {};
    AthorGitlabMergeRequestModel* _athorGitlabMergeRequest = nullptr;

    void publish( const int& projectId, const int& mergeIid );

    bool hasDiscussionByBody( const QString& body ) const;

    void checkConfigIsOk( const int& projectId, const int& mergeIid ) const;

};

ATHOR_CODE_REVIEW_API_END_NAMESPACE

#endif // ATHORCODEREVIEWCHECK_H
