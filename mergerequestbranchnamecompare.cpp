#include "mergerequestbranchnamecompare.h"

#include <QDebug>

#include "athorcodereviewpublishmodel.h"

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

QList<AthorCodeReviewDiscussionModel*> MergeRequestBranchNameCompare::check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const {

    QList<AthorCodeReviewDiscussionModel*> discussionList = {};

    checkConfigIsOk( athorGitlabMergeRequestModel );
    QString source = athorGitlabMergeRequestModel->sourceBranch();
    QString target = athorGitlabMergeRequestModel->targetBranch();

    if ( source != target ) {
        AthorCodeReviewDiscussionModel* discussion = new AthorCodeReviewDiscussionModel();
        discussion->setBody( QString( "Merge aberto para branch's diferentes ( %0 != %1 )" ).arg( source ).arg( target ) );

        discussionList.append( discussion );
    }

    return discussionList;

}

void MergeRequestBranchNameCompare::checkConfigIsOk( const gitlab::api::AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const {
    qDebug() << "MergeRequestBranchNameCompare::checkConfigIsOk";

    if ( !athorGitlabMergeRequestModel ) {
        qCritical() << "MergeRequestBranchNameCompare::checkConfigIsOk request model";
        throw new std::runtime_error( "gitlab merge request model is invalid" );
    }

    if ( athorGitlabMergeRequestModel->sourceBranch().isEmpty() ) {
        qCritical() << "MergeRequestBranchNameCompare::checkConfigIsOk source branch";
        throw new std::runtime_error( "source branch is empty" );
    }

    if ( athorGitlabMergeRequestModel->targetBranch().isEmpty() ) {
        qCritical() << "MergeRequestBranchNameCompare::checkConfigIsOk source target";
        throw new std::runtime_error( "target branch is empty" );
    }
}

ATHOR_CODE_REVIEW_API_END_NAMESPACE
