#ifndef MERGEREQUESTBRANCHNAMECOMPARE_H
#define MERGEREQUESTBRANCHNAMECOMPARE_H

#include <athor-code-review_global.h>

#include <data/athorgitlabmergerequestmodel.h>

using athor::gitlab::api::AthorGitlabMergeRequestModel;

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

class AthorCodeReviewDiscussionModel;

class MergeRequestBranchNameCompare {

public:
    static constexpr const char* TYPE = "merge-request-branch";

    QList<AthorCodeReviewDiscussionModel*> check( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const;

private:
    void checkConfigIsOk( const AthorGitlabMergeRequestModel* athorGitlabMergeRequestModel ) const;

};

ATHOR_CODE_REVIEW_API_END_NAMESPACE

#endif // MERGEREQUESTBRANCHNAMECOMPARE_H
