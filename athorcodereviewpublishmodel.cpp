#include "athorcodereviewpublishmodel.h"

ATHOR_CODE_REVIEW_API_BEGIN_NAMESPACE

AthorCodeReviewPublishModel::AthorCodeReviewPublishModel() {}

AthorCodeReviewPublishModel::~AthorCodeReviewPublishModel() {
    qDeleteAll( _discussionList );
    _discussionList = {};
}

QList<AthorCodeReviewDiscussionModel*> AthorCodeReviewPublishModel::discussionList() const {
    return _discussionList;
}

void AthorCodeReviewPublishModel::addDiscussion( AthorCodeReviewDiscussionModel* discussion ) {
    _discussionList.append( discussion );
}

void AthorCodeReviewPublishModel::setDiscussionList( const QList<AthorCodeReviewDiscussionModel*>& discussionList ) {
    _discussionList = discussionList;
}

ATHOR_CODE_REVIEW_API_END_NAMESPACE
